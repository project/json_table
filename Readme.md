# Json table
This field will be stored in Mysql database with JSON data type
(no test for other like Postgre type jsonB).
It'll be practiced for developer queries json with mysql, not like text type.

### How to use
After installing the module, create field json table.
In field settings enable default value.
In widget setting you can lock table with default value, and custom field type.
You can active button load file csv.
You can custom input of table by Example:
```yaml
col:
  2:
    type: number
row:
  3:
    type: select
    options:
      foo: foo
      foo2: foo2
rowcol:
  1,2:
    type: date
```
This means all input of 2 columns are numbers,
all 3 input rows are select,
inputs in row 1 and column 2 are date type

### Shortcut
- Ctrl + D → Duplicate row
- Alt + N → New row
- Ctrl + K → Delete current row
- Ctrl + ↑ / ↓ → Move row to up / down with arrow keys
- Arrow key → Move with arrow key

### Formatter
You can display data as table (bootstraptable, datatable ), json,
or chart (google chart or Chartjs)

### Support:
Some js addon libraries don't mix it in widget and formatter
- gojs Flowchart
- x-spreadsheet
- LuckySheet
