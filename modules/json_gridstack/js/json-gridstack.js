/**
 * @file
 * Json table behaviors.
 */
(function ($, Drupal, once) {

  'use strict';
  function saveGrid(grid, textarea, data) {
    let serializedData = grid.save(false, false);
    let $textarea = $('#' + textarea);
    if ($textarea.length) {
      $textarea.val(JSON.stringify(serializedData, null, '  '));
    } else if(data.entityType){
      // Save to localStorage.
      let gridstack = JSON.parse(localStorage.getItem('gridstack')) || {};
      if (!gridstack[data.entityType]) gridstack[data.entityType] = {};
      if (!gridstack[data.entityType][data.entityId]) gridstack[data.entityType][data.entityId] = {};
      if (!gridstack[data.entityType][data.entityId][data.fieldName]) gridstack[data.entityType][data.entityId][data.fieldName] = {};
      gridstack[data.entityType][data.entityId][data.fieldName][data.delta] = serializedData;
      localStorage.setItem('gridstack', JSON.stringify(gridstack));
    }
  }
  function addEvents(grid, textareaId) {
    grid.on('dropped', function(event, previousNode, newNode) {
      let data = newNode.el.dataset;
      saveGrid(grid, textareaId);
      let urlBlockContent = drupalSettings.json_gridstack.blockContent + '/' + data.delta;
      $.get(urlBlockContent, function (data) {
        newNode.el.classList.remove('col-auto', 'border', 'rounded', 'me-2', 'mb-2');
        const itemStack = newNode.el.querySelector('.grid-stack-item-content');
        itemStack.innerHTML = data;
        $(itemStack).find('.hidden').removeClass('hidden');
        $(itemStack).find('.btn-close').on('click', function () {
          const el = this.closest('.block');
          if (el) {
            el.remove();
            grid.removeWidget(el);
            saveGrid(grid, textareaId);
          }
        });
      });
    })
    .on('removed resizestop change', function(event, previousNode, newNode) {
      let serializedData = grid.save(false, false);
      $(`#${textareaId}`).val(JSON.stringify(serializedData));
      saveGrid(grid, textareaId, event.target.dataset);
    });
  }
  Drupal.behaviors.jsonBlockGridstack = {
    attach (context, settings) {
      const options = {
        float: false,
        cellHeight: 'auto',
        acceptWidgets: true,
        cellHeightThrottle: 100,
        handle: '.card-header',
        staticGrid: false,
        removable: true,
        animate: true,
        column: 6,
        columnOpts: {
          columnWidth: 100, // wanted width
        },
      };
      once('grid-stack', '.grid-stack').forEach(function (element) {

        function removeWidget(el) {
          let id = el.attr('id')
          el.remove();
          grid.removeWidget(el[0], false);
          let serializedData = grid.save(false, false);
          serializedData = serializedData.filter((block) => block.id !== id);
          $(`#${textareaId}`).val(JSON.stringify(serializedData));
        }

        // Load from localStorage.
        let data = element.dataset || null;
        if (data.entityType) {
          let gridstack = JSON.parse(localStorage.getItem('gridstack')) || {};
          let serializedData = gridstack?.[data.entityType]?.[data.entityId]?.[data.fieldName]?.[data.delta] || null;
          if(serializedData) {
            serializedData.forEach((item)=>{
              let block = $(element).find(`.grid-stack-item[gs-id="${item.id}"]`);
              block.attr('gs-x', item.x);
              block.attr('gs-y', item.y);
              block.attr('gs-w', item.w);
              block.attr('gs-h', item.h);
            });
          }
        }
        const grid = GridStack.init(options);
        let gridId = element.id;
        let textareaId = $('#' + gridId).parent().find('textarea').attr('id');
        addEvents(grid, textareaId);
        GridStack.setupDragIn('.list-block .grid-stack-item', {appendTo: '.grid-stack',});
        if ($(`#${textareaId}`).val() != undefined) {
          $('.grid-stack-item .btn-close').removeClass('hidden').on('click', function () {
            removeWidget($(this).closest('.grid-stack-item'));
          });
        }
      });

    }
  };

} (jQuery, Drupal, once));
