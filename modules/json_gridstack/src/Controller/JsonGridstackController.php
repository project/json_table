<?php

namespace Drupal\json_gridstack\Controller;

use Drupal\Core\Block\BlockManagerInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Plugin\Context\LazyContextRepository;
use Drupal\Core\Render\RendererInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;

/**
 * Returns responses for Json gridstack routes.
 */
class JsonGridstackController extends ControllerBase {

  /**
   * The controller constructor.
   *
   * @param \Symfony\Component\HttpFoundation\RequestStack $requestStack
   *   Resquest service.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   Render service.
   * @param \Drupal\Core\Block\BlockManagerInterface $blockManager
   *   Block plugin service.
   * @param \Drupal\Core\Plugin\Context\LazyContextRepository $contextRepository
   *   Contextual service.
   */
  public function __construct(
    protected RequestStack $requestStack,
    protected RendererInterface $renderer,
    protected BlockManagerInterface $blockManager,
    protected LazyContextRepository $contextRepository,
  ) {}

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self {
    return new self(
      $container->get('request_stack'),
      $container->get('renderer'),
      $container->get('plugin.manager.block'),
      $container->get('context.repository'),
    );
  }

  /**
   * Show block content.
   *
   * @inheritDoc
   */
  public function showBlockContent($block_id = '') {
    $currentUser = $this->currentUser();
    $build = $this->renderGridStackBlock($currentUser, $block_id);
    return new Response($this->renderer->render($build));
  }

  /**
   * Render grid stack block.
   *
   * @inheritDoc
   */
  public function renderGridStackBlock($currentUser, $delta) {

    $plugin_block = $this->blockManager->createInstance($delta, []);
    $render = '';
    if ($plugin_block->access($currentUser)) {
      $render = $plugin_block->build();
      if (!empty($render["#view"]) && empty($render["#view"]->result)) {
        return FALSE;
      }
      if (empty($render)) {
        return FALSE;
      }
    }
    $block = $this->blockManager->getDefinition($delta);

    return [
      '#theme' => 'json_gridstack_block',
      '#id' => str_replace(':', '-', $delta),
      '#delta' => $delta,
      '#collapse' => FALSE,
      '#title' => $block['admin_label'] ?? $this->t('Missing block'),
      '#content' => $render,
      '#weight' => 0,
      '#h' => $h ?? 0,
      '#x' => $x ?? 0,
      '#y' => $y ?? 0,
      '#w' => $w ?? 0,
    ];
  }

}
