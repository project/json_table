<?php

namespace Drupal\json_gridstack\Plugin\Field\FieldFormatter;

use Drupal\Core\Block\BlockManagerInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Security\TrustedCallbackInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'GridStack blocks' formatter.
 *
 * @FieldFormatter(
 *   id = "json_gridstack_blocks",
 *   label = @Translation("GridStack blocks"),
 *   field_types = {"json"},
 * )
 */
class JsonGridStackBlocksFormatter extends FormatterBase implements ContainerFactoryPluginInterface, TrustedCallbackInterface {

  /**
   * {@inheritdoc}
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, $label, $view_mode, array $third_party_settings, protected BlockManagerInterface $blockManager) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    // @see \Drupal\Core\Field\FormatterPluginManager::createInstance().
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('plugin.manager.block'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings(): array {
    $setting = ['attach_library' => ''];
    return $setting + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state): array {
    $elements['attach_library'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Attach library'),
      '#default_value' => $this->getSetting('attach_library'),
    ];
    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary(): array {
    return [
      $this->t('Attach library: @libraries', ['@libraries' => $this->getSetting('attach_library')]),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode): array {
    $element = [];
    $entity = $items->getEntity();
    foreach ($items as $delta => $item) {
      $content = '';
      if (!empty($item->value)) {
        $blockValue = json_decode($items[$delta]->value, TRUE);
        $content = [
          '#type' => 'container',
          '#attributes' => [
            'class' => ['grid-stack', 'vh-100'],
            'id' => $items->getName() . '-' . $delta,
            'data-entity-id' => $entity->id(),
            'data-entity-type' => $entity->getEntityTypeId(),
            'data-field-name' => $items->getName(),
            'data-delta' => $delta,
          ],
        ];

        foreach ($blockValue as $blockStack) {
          $plugin_block = $this->blockManager->createInstance($blockStack['id'], []);
          $render = $plugin_block->build();
          $blockDefinition = $plugin_block->getPluginDefinition();
          $content[] = [
            '#theme' => 'json_gridstack_block',
            '#id' => $blockStack['id'] ?? '',
            '#collapse' => FALSE,
            '#title' => $blockDefinition['admin_label'] ?? $this->t('Missing block'),
            '#content' => $render,
            '#weight' => 0,
            '#h' => $blockStack['h'] ?? 0,
            '#x' => $blockStack['x'] ?? 0,
            '#y' => $blockStack['y'] ?? 0,
            '#w' => $blockStack['w'] ?? 0,
          ];
        }
      }

      $element[$delta] = $content;
    }

    $element['#attached'] = [
      'library' => ['json_gridstack/json_gridstack'],
      'drupalSettings' => [
        'json_gridstack' => [
          'blockContent' => Url::fromRoute('json_gridstack.block')->toString(),
        ],
      ],
    ];
    if (!empty($this->getSetting('attach_library'))) {
      $attaches = explode(',', $this->getSetting('attach_library'));
      foreach ($attaches as $attach) {
        $element['#attached']['library'] = trim($attach);
      }
    }
    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public static function trustedCallbacks() {
    return ['postRender'];
  }

}
