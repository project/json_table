<?php

namespace Drupal\json_table\Plugin\Field\FieldType;

use Drupal\Core\Field\Attribute\FieldType;
use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\Core\Url;

/**
 * Field type of json.
 */
#[FieldType(
  id: "json",
  label: new TranslatableMarkup("Json table field"),
  description: new TranslatableMarkup("Field to store a json."),
  default_widget: "json_table_widget",
  default_formatter: "json_tables_formatter",
)]
class JsonItem extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return [
      // Columns contains the values that the field will store.
      'columns' => [
        'value' => [
          'type' => 'json',
          'pgsql_type' => 'json',
          'mysql_type' => 'json',
          'not null' => FALSE,
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    $value = $this->get('value')->getValue();
    return $value === NULL || $value === '';
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultFieldSettings() {
    return [
      // Declare a single setting, 'size', with a default value of 'large'.
      'size' => 'big',
      'lock_values' => FALSE,
      'customtype' => '',
    ] + parent::defaultFieldSettings();
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties['value'] = DataDefinition::create('string')
      ->setLabel(t('JSON value'));
    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public function fieldSettingsForm(array $form, FormStateInterface $form_state) {

    $element = [];
    // The key of the element should be the setting name.
    $element['size'] = [
      '#title' => $this->t('Size'),
      '#type' => 'select',
      '#options' => [
        'normal' => $this->t('Small'),
        'medium' => $this->t('Medium'),
        'big' => $this->t('Large'),
      ],
      '#default_value' => $this->getSetting('size'),
    ];
    $element['lock_values'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Lock cell default values from further edits during node add/edit. Most commonly used to have fixed values for the header.'),
      '#description' => $this->t('Use only for widget Json table'),
      '#default_value' => $this->getSetting('lock_values'),
    ];
    $element['customtype'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Custom configured format for columns or rows'),
      '#default_value' => $this->getSetting('customtype') ?? '',
      '#description' => $this->t("Use validate format yml. Only for Json table data that does not use a file, entity reference type, <a href='@help' class='use-ajax' data-dialog-type='modal' data-dialog-option=\"{&quot;width&quot;: &quot;80%&quot;}\">read more</a>", ['@help' => Url::fromRoute('help.page', ['name' => 'json_table'])->toString()]),
      '#attributes' => [
        'data-yaml-editor' => 'true',
      ],
    ];
    $element['#attached']['library'][] = 'json_table/config_yaml_editor';
    $form_state->setValue('set_default_value', TRUE);
    return $element;
  }

}
