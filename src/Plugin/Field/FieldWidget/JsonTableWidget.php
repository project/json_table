<?php

namespace Drupal\json_table\Plugin\Field\FieldWidget;

use Drupal\Component\Utility\Html;
use Drupal\Core\Field\Attribute\FieldWidget;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Markup;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Yaml\Yaml;

/**
 * Plugin implementation of the 'json_editor_widget' widget.
 */
#[FieldWidget(
  id: 'json_table_widget',
  label: new TranslatableMarkup('Json table'),
  field_types: [
    'json',
  ],
)]
class JsonTableWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, array $third_party_settings, protected RendererInterface $renderer) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $third_party_settings);
  }

  /**
   * {@inheritdoc}
   */

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new self(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['third_party_settings'],
      $container->get('renderer'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'header' => '',
      'customtype' => '',
      'lock_values' => TRUE,
      'lock_button' => FALSE,
      'load_file' => FALSE,
      'ajax' => FALSE,
      'associative' => FALSE,
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritDoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $field_name = $items->getName();
    $id = Html::getUniqueId($field_name . '-' . $delta);
    // Decode JSON data only if AJAX is enabled.
    $valuesRow = [];
    $value = $items[$delta]?->value ?? NULL;
    $default_value = $this->fieldDefinition->getDefaultValueLiteral();
    if (empty($value) && !empty($default_value[$delta])) {
      $value = $default_value[$delta]['value'] ?? '';
      $items[$delta]->value = $value;
    }
    if (!$this->getSetting('ajax') && !empty($items[$delta]->value)) {
      $valuesRow = json_decode($items[$delta]->value, TRUE);
      // Sometimes we get data from X-speadsheet.
      if (!empty($valuesRow['rows'])) {
        $valuesRow = $this->convertSpreadSheet($valuesRow['rows']);
        $value = json_encode($valuesRow);
      }
    }
    // Render table container.
    $header = $this->convertTableHeader($this->getSetting('header'));
    $container = $this->renderTable($id, $valuesRow, $header);
    $fieldSettings = $this->fieldDefinition->getSettings();
    $lockValues = $this->getSetting('lock_values') ?: $fieldSettings['lock_values'];
    // Render the container if AJAX is enabled.
    if ($this->getSetting('ajax')) {
      $container = Markup::create($this->renderer->render($container));
    }
    $associative = '';
    if ($this->getSetting('associative') && !empty($header)) {
      $tmp = [];
      foreach ($header as $label) {
        $tmp[] = HTML::getClass(trim($label));
      }
      $associative = implode('|', $tmp);
    }
    $elements['value'] = [
      '#title' => $this->fieldDefinition->getLabel(),
      '#type' => 'textarea',
      '#default_value' => $value,
      '#description' => $container,
      '#attributes' => [
        'data-json-table' => $this->getSetting('mode'),
        'data-lock-values' => $lockValues,
        'data-lock-button' => $this->getSetting('lock_button'),
        'data-load-file' => $this->getSetting('load_file'),
        'data-json-associative' => $associative,
        'class' => ['json-table', 'js-hide', $this->getSetting('mode')],
      ],
      '#attached' => [
        'library' => ['json_table/json_table'],
        'drupalSettings' => [
          'json_editor' => [$delta => $this->getSetting('mode')],
        ],
      ],
      '#element_validate' => [
        [$this, 'validateJsonData'],
      ],
    ];
    return $elements;
  }

  /**
   * {@inheritDoc}
   */
  private function convertTableHeader($header) {
    if (empty($header)) {
      return [];
    }
    $checkJson = json_decode($header, TRUE);
    if (!$checkJson) {
      $header = str_replace([
        "\r\n",
        "\r",
        "\t",
        ', ',
        ',',
        '; ',
        ';',
      ], PHP_EOL, $this->getSetting('header'));
      $header = explode(PHP_EOL, $header);
    }
    elseif (is_array($checkJson)) {
      $header = $checkJson;
    }
    return $header;
  }

  /**
   * {@inheritDoc}
   */
  public function renderTable($id, $data, $header) {
    $textPaste = $this->t('Paste your Excel data here');
    $buttons = [
      '<button type="button" class="button button-action button--primary btn btn-primary add-row">' . $this->t('Add row') . '</button>',
      '<button type="button" class="button button-action button--primary btn btn-primary add-col">' . $this->t('Add column') . '</button>',
      '<button type="button" class="button button--danger btn btn-danger remove-row">- ' . $this->t('Remove row') . '</button>',
      '<button type="button" class="button button--danger btn btn-danger remove-col">- ' . $this->t('Remove column') . '</button>',
    ];
    if ($this->getSetting('load_file')) {
      $buttons[] = '<button type="button" class="button button--primary btn btn-success load-file">📂 ' . $this->t('csv') . '</button>';
    }
    $container = [
      '#type' => 'container',
      '#attributes' => [
        'class' => [
          'json-table-wrapper',
        ],
      ],
      'btn' => [
        '#markup' => Markup::create(implode('', $buttons)),
        '#weight' => 0,
      ],
      'table' => [
        '#type' => 'table',
        '#header' => $header,
        '#attributes' => [
          'id' => $id,
          'class' => [
            'json-table',
          ],
        ],
        '#weight' => 1,
      ],
    ];
    $fieldSettings = $this->fieldDefinition->getSettings();
    $custom_type = $this->getSetting('customtype') ?: $fieldSettings['customtype'] ?? '';
    if (!empty($custom_type)) {
      $custom_type = Yaml::parse($custom_type);
    }
    if (empty($data)) {
      $inputRow = [
        '#type' => 'textfield',
        '#title' => '',
        '#title_display' => 'invisible',
        '#attributes' => [
          'class' => ['paste', 'form-control', 'form-text', 'form-element'],
        ],
        '#maxlength' => NULL,
        '#size' => NULL,
      ];
      $rowDefault = [$inputRow];
      if (!empty($header)) {
        foreach ($header as $col => $label) {
          $rowDefault[$col] = $inputRow;
          $rowDefault[$col]['#attributes']['title'] = $label;
          if (!empty($custom_type['col'])) {
            if (!empty($custom_type['col'][$col]['type'])) {
              $rowDefault[$col]['#type'] = $custom_type['col'][$col]['type'];
            }
            if (!empty($custom_type['col'][$col]['attributes'])) {
              $rowDefault[$col]['#attributes'] = [
                ...$custom_type['col'][$col]['attributes'],
                ...$rowDefault[$col]['#attributes'],
              ];
            }
          }
        }
      }
      if ($rowDefault[0]['#type'] == 'textfield') {
        $rowDefault[0]['#attributes']['placeholder'] = $textPaste;
      }
      $container['table'] += [$rowDefault];
    }
    else {
      $lock_values = $this->getSetting('lock_values') ?: $fieldSettings['lock_values'];
      $default_value = $this->fieldDefinition->getDefaultValueLiteral();
      if ($lock_values) {
        if (!empty($default_value)) {
          $default_value = Yaml::parse(current($default_value)['value']);
        }
        unset($container['btn']['#markup']);
      }
      $shortcuts = [
        $this->t('- Ctrl + D: Duplicate row'),
        $this->t('- Alt + N: New row'),
        $this->t('- Ctrl + K: Delete current row'),
        $this->t('- Ctrl + ↑ / ↓: Move row to up / down'),
        $this->t('- Arrow key: Move with arrow key'),
      ];
      $btn = '<span role="button" class="shortcut toolbar-item" title="' . implode(PHP_EOL, $shortcuts) . '">❓</span>';
      if (!empty($this->getSetting('lock_button'))) {
        $lock = $this->t('Lock table');
        $btn .= '<span role="button" class="lock toolbar-item" data-lock="active" title="' . $lock . '">🔒</span>';
      }
      $container['btn']['#markup'] = Markup::create('<div class="text-align-right toolbar">' . $btn . '</div>');
      foreach ($data as $x => $row) {
        if (!is_array($row)) {
          continue;
        }
        foreach ($row as $y => $col) {
          $type = is_numeric($col) ? 'number' : 'textfield';
          $container['table'][$x][$y] = [
            '#type' => $type,
            '#title' => '',
            '#title_display' => 'invisible',
            '#default_value' => is_string($col) ? $col : '',
            '#value' => is_string($col) ? $col : '',
            '#attributes' => [
              'class' => [
                'form-control',
                'form-text',
                'form-element',
                'row-' . $x,
                'col-' . $y,
              ],
            ],
          ];

          if ($lock_values && isset($default_value[$x][$y]) && $default_value[$x][$y] != '') {
            $container['table'][$x][$y]['#attributes']['class'][] = 'js-hide';
            $container['table'][$x][$y]['#maxlength'] = 1024;
            $container['table'][$x][$y]['#default_value'] = $default_value[$x][$y];
            $container['table'][$x][$y]['#value'] = $default_value[$x][$y];
            $container['table'][$x][$y]['#title'] = $default_value[$x][$y];
            unset($container['table'][$x][$y]['#title_display']);
          }
          if (!empty($custom_type["row"][$x])) {
            foreach ($custom_type["row"][$x] as $att => $attVal) {
              if (in_array($att, ['field_prefix', 'field_suffix', 'type']) && $lock_values && isset($default_value[$x][$y]) && $default_value[$x][$y] != '') {
                continue;
              }
              $container['table'][$x][$y]['#' . $att] = $attVal;
            }
          }
          if (!empty($custom_type["col"][$y])) {
            foreach ($custom_type["col"][$y] as $att => $attVal) {
              if (in_array($att, ['field_prefix', 'field_suffix', 'type']) && $lock_values && isset($default_value[$x][$y]) && $default_value[$x][$y] != '') {
                continue;
              }
              $container['table'][$x][$y]['#' . $att] = $attVal;
            }
          }
          if (!empty($custom_type["rowcol"]["$x,$y"])) {
            foreach ($custom_type["rowcol"]["$x,$y"] as $att => $attVal) {
              if (in_array($att, ['field_prefix', 'field_suffix', 'type']) && $lock_values && isset($default_value[$x][$y]) && $default_value[$x][$y] != '') {
                continue;
              }
              $container['table'][$x][$y]['#' . $att] = $attVal;
            }
          }
          // Select must have empty option.
          if ($container['table'][$x][$y]['#type'] == 'select') {
            $container['table'][$x][$y]['#empty_option'] = $this->t('- None -');
            $container['table'][$x][$y]['#empty_value'] = '';
            $container['table'][$x][$y]['#options'] = ['' => $this->t('- None -')] + $container['table'][$x][$y]['#options'];
          }
          // Checkbox must have value ''.
          if ($container['table'][$x][$y]['#type'] == 'select') {
            if ($container['table'][$x][$y]['#default_value'] == 0) {
              $container['table'][$x][$y]['#default_value'] = '';
            }
          }
          if ($lock_values && !empty($container['table'][$x][$y]['#default_value'])) {
            switch ($container['table'][$x][$y]['#type']) {
              case 'checkbox':
              case 'number':
                if (!is_numeric($container['table'][$x][$y]['#default_value'])) {
                  $container['table'][$x][$y]['#type'] = 'textfield';
                }
                break;
            }
          }
        }
      }
    }
    return $container;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $elements['header'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Defined header'),
      '#description' => $this->t('You can set the header separated by tab, comma ",", semicolon ";" or with a json object if you want switch to json viewer, for example:') .
      json_encode(['name' => "Col Name", 'age' => 'Col Age']),
      '#default_value' => $this->getSetting('header'),
    ];
    $fieldSettings = $this->fieldDefinition->getSettings();
    $elements['customtype'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Custom configured format for columns or rows'),
      '#default_value' => $this->getSetting('customtype') ?: $fieldSettings['customtype'] ?? '',
      '#description' => $this->t("Use validate format yml. Only for data that does not use a file, entity reference type <a href='@help' class='use-ajax' data-dialog-type='modal' data-dialog-option=\"{&quot;width&quot;: &quot;80%&quot;}\">read more</a>", ['@help' => Url::fromRoute('help.page', ['name' => 'json_table'])->toString()]),
      '#attributes' => [
        'data-yaml-editor' => 'true',
      ],
    ];
    $elements['lock_values'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Lock cell default values from further edits during node add/edit. Most commonly used to have fixed values for the header.'),
      '#default_value' => $this->getSetting('lock_values') ?: $fieldSettings['lock_values'],
    ];
    $elements['lock_button'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show button lock 🔒'),
      '#default_value' => $this->getSetting('lock_button'),
      '#states' => [
        'visible' => [
          ':input[name$="settings_edit_form][settings][lock_values]"]' => ['checked' => TRUE],
        ],
      ],
    ];
    $elements['load_file'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show button load file csv 📂'),
      '#default_value' => $this->getSetting('load_file'),
    ];
    $elements['ajax'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Ajax load'),
      '#description' => $this->t('For high performance, use if you have a large JSON table'),
      '#default_value' => $this->getSetting('ajax'),
    ];
    $elements['associative'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Save JSON to associative'),
      '#description' => $this->t('Convert JSON to associative array with header'),
      '#default_value' => $this->getSetting('associative'),
    ];
    $elements['#attached']['library'][] = 'json_table/config_yaml_editor';
    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    if (!empty($this->getSetting('header'))) {
      $summary[] = $this->t('Custom header');
    }
    if (!empty($this->getSetting('ajax'))) {
      $summary[] = $this->t('Load data table with javascript');
    }
    return $summary;
  }

  /**
   * Check the submitted JSON against the configured JSON Schema.
   *
   * {@inheritdoc}
   */
  public static function validateJsonData($element, FormStateInterface $form_state) {
    json_decode($element['#value']);
    return json_last_error() == JSON_ERROR_NONE;
  }

  /**
   * Convert data type XspreadSheet to array data.
   *
   * {@inheritdoc}
   */
  protected function convertSpreadSheet(mixed $rows): array {
    $data = [];
    foreach ($rows as $row) {
      if (!is_array($row)) {
        continue;
      }
      $cells = [];
      foreach ($row['cells'] as $cell) {
        $cells[] = $cell['text'];
      }
      if (!empty($cells)) {
        $data[] = $cells;
      }
    }
    return $data;
  }

  /**
   * Set to null if this is the default equal value.
   *
   * {@inheritdoc}
   */
  public function massageFormValues(array $values, array $form, FormStateInterface $form_state) {
    if (!empty($form_state->getValue('default_value_input'))) {
      return $values;
    }
    $field_definition = $this->fieldDefinition;
    $default_value = $field_definition->getDefaultValueLiteral();
    foreach ($values as $delta => &$value) {
      if (!isset($default_value[$delta])) {
        break;
      }
      if (str_replace(' ', '', $default_value[$delta]['value']) == str_replace(' ', '', $value['value'])) {
        $value['value'] = NULL;
      }
    }
    return $values;
  }

}
