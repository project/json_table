/**
 * @file
 * Custom JS for the JSON table field formatter.
 */

(function ($, Drupal, once) {
  'use strict';

  /**
   * Attach behavior for JSON Fields.
   *
   * @type {Drupal~behavior}
   */
  function loadData(that){
    let data = that.find('textarea.json-table').val();
    let tbody = that.find('table.json-table tbody');
    let countTr = tbody.find('tr').length;
    if (data !== '' && countTr === 1) {
      tbody.html('');
      let dataTable = JSON.parse(data);
      if(typeof dataTable === 'object') {
        dataTable = Object.values(dataTable);
      }
      dataTable.forEach((rows,row) => {
        if(typeof rows === 'object') {
          rows = Object.values(rows);
        }
        let tr = $('<tr></tr>');
        rows.forEach((v3,col) => {
          let cell = 'cell-' + row + '-' + col;
          let td = $('<td><input class="form-control form-element form-text ' + cell + '" value="' + v3 + '"/></td>');
          td.appendTo(tr);
        });
        tr.appendTo(tbody);
      });
    }
  }
  function getHeaderArray(that){
    var header = [];
    let $table = that.closest('table'),
      header_associative = $table.data('associative');
    if(header_associative === '1'){
      // This loop is not very efficient. It crashes browser.
      $table.find('thead th').each(function (key,value) {
        header.push(value.innerHTML);
      });
    }
    return header;
  }
  function convertTable2Json(that) {
    let $textarea = that.closest('.js-form-type-textarea').find('textarea.json-table');
    if(!$textarea.length) {
      $textarea = that.closest('.description').parent().find('textarea.json-table');
    }
    let table = [];
    let associative = $textarea.data('json-associative');
    let header = associative === '' ? getHeaderArray(that) : associative.split('|');
    that.closest('table').find('tbody tr').each(function (row, tr) {
      let rows = $(this).find('td');
      table[row] = {};
      $.each(rows, function (col, value) {
        let associative = col;
        if(header[col] !== undefined){
          associative = header[col];
        }
        table[row][associative] = $(this).find('input,select,textarea').val();
      });
    });
    $textarea.val(JSON.stringify(table));
  }

  function keyupInputTable(selector = null) {
    if (selector === null) {
      selector = '.json-table input,.json-table select,.json-table textarea';
    }
    if ($(selector).length) {
      $(once('json-table-input', selector)).on('change keyup', function (e) {
        convertTable2Json($(this))
      });
    } else {
      // Dom is not ready will reload in 5s.
      setTimeout(function () {
        keyupInputTable(selector);
      }, 3000);
    }
  }

  function shortcut(table) {
    table.on("keydown", function (e) {
      const focusedElement = $(":focus");
      const currentCell = focusedElement.closest("td");
      const currentRow = focusedElement.closest("tr");
      if (!currentRow.length || !table.has(currentRow).length) return;

      switch (true) {
        case e.ctrlKey && e.key === "d":
          e.preventDefault();
          duplicateRow(currentRow);
          break;

        case e.altKey && e.key === "n":
          e.preventDefault();
          addNewRow(table);
          break;

        case e.ctrlKey && e.key === "k":
          e.preventDefault();
          removeRow(currentRow, table);
          break;

        case e.ctrlKey && e.key === "ArrowUp":
          e.preventDefault();
          moveRowUp(currentRow);
          break;

        case e.ctrlKey && e.key === "ArrowDown":
          e.preventDefault();
          moveRowDown(currentRow);
          break;

        case e.key === "ArrowUp":
          e.preventDefault();
          focusCellAbove(currentCell);
          break;

        case e.key === "ArrowDown":
          e.preventDefault();
          focusCellBelow(currentCell);
          break;

        case e.key === "ArrowLeft":
          e.preventDefault();
          focusCellLeft(currentCell);
          break;

        case e.key === "ArrowRight":
          e.preventDefault();
          focusCellRight(currentCell);
          break;
      }
      convertTable2Json(table);
    });
  }
  function duplicateRow(currentRow) {
    let clonedRow = currentRow.clone();
    clonedRow.insertAfter(currentRow);
    clonedRow.find("input").first().focus();
  }
  function addNewRow(table) {
    let columns = table.find("tr").first().find('td').length;
    let total = table.find("tr").length;
    let newRow = $("<tr>");
    for (let i = 0; i < columns; i++) {
      newRow.append(
        $("<td>").append(
          $("<input>", {
            type: "text",
            class: `form-control form-element form-text cell-${total}-${i}`,
          })
        )
      );
    }
    table.append(newRow);
    newRow.find("input").first().focus();
  }
  function removeRow(currentRow) {
    currentRow.remove();
  }

  function moveRowUp(currentRow) {
    let prevRow = currentRow.prev();
    if (prevRow.length) {
      currentRow.insertBefore(prevRow);
    }
  }
  function moveRowDown(currentRow) {
    let nextRow = currentRow.next();
    if (nextRow.length) {
      currentRow.insertAfter(nextRow);
    }
  }
  function focusCellAbove(currentCell) {
    let colIndex = currentCell.index();
    let prevRow = currentCell.closest("tr").prev();
    if (prevRow.length) {
      prevRow.find("td").eq(colIndex).find("input").focus();
    }
  }
  function focusCellBelow(currentCell) {
    let colIndex = currentCell.index();
    let nextRow = currentCell.closest("tr").next();
    if (nextRow.length) {
      nextRow.find("td").eq(colIndex).find("input").focus();
    }
  }
  function focusCellLeft(currentCell) {
    let prevCell = currentCell.prev();
    if (prevCell.length) {
      prevCell.find("input").focus();
    }
  }
  function focusCellRight(currentCell) {
    let nextCell = currentCell.next();
    if (nextCell.length) {
      nextCell.find("input").focus();
    }
  }

  keyupInputTable();
  Drupal.behaviors.json_table = {
    attach: function (context) {
      // Initialize the Quick Edit app once per page load.
      $(once('json-table-field', '.field--type-json.field--widget-json-table-widget', context)).each(function () {
        $(once('table-add-row', '.add-row', context)).on('click', function () {
          let tbody = $(this).closest('.json-table-wrapper').find('table tbody');
          let tr = tbody.children().last().clone();
          tbody.append(tr);
          pasteFromClipBoard();
          shortcut(tbody);
          keyupInputTable($(this).parent().find('.json-table input,.json-table select,.json-table textarea'));
          convertTable2Json(tbody);
        });
        $(once('table-add-col', '.add-col', context)).on('click', function () {
          let tbody = $(this).closest('.json-table-wrapper').find('table tbody');
          tbody.find('tr').append('<td><input class="form-control form-element form-text"/></td>');
          keyupInputTable($(this).parent().find('.json-table input,.json-table select,.json-table textarea'));
          convertTable2Json(tbody);
          shortcut(tbody);
        });
        $(once('table-remove-row', '.remove-row', context)).on('click', function () {
          let tbody = $(this).closest('.json-table-wrapper').find('table tbody');
          tbody.find('tr:last').remove();
          convertTable2Json(tbody);
        });
        $(once('table-remove-col', '.remove-col', context)).on('click', function () {
          let tbody = $(this).closest('.json-table-wrapper').find('table tbody');
          tbody.find('tr td:last-child').remove();
          convertTable2Json(tbody);
        });
        pasteFromClipBoard();
        shortcut($(this).find('table tbody'));
        keyupInputTable();
        loadData($(this));
        function pasteFromClipBoard() {
          $(once('paste','.json-table input.paste')).on('paste', function (e) {
            var $this = $(this),
              header = getHeaderArray($this);
            $.each(e.originalEvent.clipboardData.items, function (i, v) {
              if (v.type === 'text/plain') {
                v.getAsString(function (text) {
                  if(text.includes('\r\n') || text.includes('\t')){
                    let tbody = $this.closest('tbody');
                    let $textarea = $this.closest('.form-type-textarea').find('textarea');
                    let table = [];
                    text = text.trim('\r\n');
                    tbody.html('');
                    text.split('\r\n').forEach((v2,row) => {
                      let rows = v2.split('\t');
                      table[row] = {};
                      let tr = $('<tr></tr>');
                      rows.forEach((v3,col) => {
                        let cell = 'cell-' + row + '-' + col;
                        let td = $('<td><input class="form-control form-element form-text ' + cell + '" value="' + v3 + '"/></td>');

                        let associative = col;
                        if(header[col] !== undefined){
                          associative = header[col];
                        }
                        table[row][associative] = v3;
                        td.appendTo(tr);
                      });
                      tr.appendTo(tbody);
                    });
                    $textarea.val(JSON.stringify(table));
                  }
                });
              }
            });
          });
        }
      });

      $(once('lock', '.lock', context)).on('click', function () {
        let state = $(this).data('lock');
        let table = $(this).parent().parent().find('table tbody');
        if (state == 'active') {
          $(this).data('lock', 'inactive');
          $(this).text('🔓');
          table.find('label').addClass('js-hide');
          table.find('input.js-hide').removeClass('js-hide');
        } else {
          $(this).data('lock', 'active');
          $(this).text('🔒');
          let label = table.find('label');
          label.removeClass('js-hide');
          label.parent().find('input').addClass('js-hide');
        }
      });
    }
  };

  // Load file csv
  Drupal.behaviors.csvLoader = {
    attach: function (context) {
      $(once('csvloader','.load-file', context)).on("click", function () {
        let table = $(this).closest('.json-table-wrapper').find('table');
        const input = $("<input>", { type: "file", accept: ".csv" }).on("change", function (event) {
          handleFile(event, table);
        });
        input.trigger("click");
      });

      function handleFile(event, table) {
        const file = event.target.files[0];
        if (!file || !file.name.endsWith(".csv")) {
          alert(Drupal.t("Please select CSV file."));
          return;
        }

        const reader = new FileReader();
        reader.onload = function (e) {
          const csvContent = e.target.result;
          const jsonData = renderCsvToTable(csvContent, table);
          let $textarea = table.closest('.form-type-textarea').find('textarea');
          $textarea.val(JSON.stringify(jsonData));
          keyupInputTable();
        };
        reader.readAsText(file);
      }
      function detectDelimiter(csv) {
        const delimiters = [";", "\t", "|", ","];
        const firstLine = csv.split(/\r?\n/)[0]; // Lấy dòng đầu tiên
        return delimiters.reduce((best, delimiter) =>
            (firstLine.split(delimiter).length > best.count ? { char: delimiter, count: firstLine.split(delimiter).length } : best),
          { char: ",", count: 0 }
        ).char;
      }

      function parseCsvLine(line, delimiter) {
        const regex = new RegExp(`"([^"]*)"|([^${delimiter}]+)`, "g");
        let match, values = [];

        while ((match = regex.exec(line)) !== null) {
          values.push(match[1] !== undefined ? match[1].trim() : (match[2] || "").trim());
        }

        return values;
      }

      function renderCsvToTable(csv, table) {
        const lines = csv.trim().split(/\r?\n/);
        if (lines.length < 2) return [];

        let json = [];
        table.find("thead").empty();
        const tbody = table.find("tbody").empty();

        const delimiter = detectDelimiter(csv);
        const headers = parseCsvLine(lines[0], delimiter);

        // Save header to 2D
        json.push(headers);

        // Create (thead)
        const thead = $("<thead>").append($("<tr>").append(headers.map(header => $("<th>").text(header))));
        table.append(thead);

        for (let i = 0; i < lines.length; i++) {
          const values = parseCsvLine(lines[i], delimiter);
          json.push(values);
          // Create table HTML
          const row = $(`<tr class="row--${i}">`).append(
            values.map((value, index) => {
              let inputType = !isNaN(value) && isFinite(value) ? "number" : "text";
              if (i===0) {
                inputType = 'hidden';
              }
              return $(`<td class="col--${index}">`).append(
                $("<input>", {
                  type: inputType,
                  value: value || "",
                  class: `form-control form-element form-text cell-${i}-${index}`,
                })
              )
            })
          );
          tbody.append(row);
        }

        table.append(tbody);

        return json;
      }

    },
  };
}(jQuery, Drupal, once));
