(function (Drupal, once) {

  'use strict';

  Drupal.behaviors.yamlEditor = {
    attach: function (context) {
      const initEditor = function () {
        once('yaml-editor', 'textarea[data-yaml-editor]', context).forEach(function ($textarea) {
          let $editDiv = document.createElement('div');

          if (!$textarea.parentNode) {
            return;
          }

          $textarea.classList.add('visually-hidden');

          $textarea.parentNode.insertBefore($editDiv, $textarea);

          // Init Ace editor.
          let editor = ace.edit($editDiv);
          editor.getSession().setValue($textarea.value);
          editor.getSession().setMode('ace/mode/yaml');
          editor.getSession().setTabSize(2);
          editor.setTheme('ace/theme/chrome');
          editor.setOptions({
            minLines: 3,
            maxLines: 20
          });

          // Update Drupal textarea value.
          editor.getSession().on('change', function () {
            $textarea.value = editor.getSession().getValue();
          });
        });
      };

      // Check if Ace editor is already available and load it from source cdn otherwise.
      if (typeof ace !== 'undefined') {
        initEditor();
      }
    }
  };

})(Drupal, once);
