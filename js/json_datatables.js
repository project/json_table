/**
 * @file
 * Custom JS for the JSON table field formatter.
 */
(function ($, Drupal, drupalSettings, once) {
  'use strict';
  /**
   * Attach behavior for JSON Fields.
   *
   * @type {Drupal~behavior}
   */
  Drupal.behaviors.json_datatables = {
    attach(context) {
      $(once('json-table-datatables', '.json-table.datatables', context)).each(function (index, element) {
        let fieldName = $(this).data('json-field');
        let config = drupalSettings.json_table[fieldName];
        if ($(this).find('thead').length == 0) {
          $(this).prepend('<thead></thead>');
          let firstRow = $(this).find('tbody tr:first');
          $(this).find('thead').append(firstRow);
        }
        if (config != undefined) {
          config = {
            info: true,
            ordering: true,
            paging: true
          };
        }
        $(this).DataTable(config);
      });
    }
  };

})(jQuery, Drupal, drupalSettings, once);
